<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>



<title>Adiciona Produto</title>
</head>
<body>
<div class="container">

<h3>Adicionar Produto</h3>
<form method="post" action="../controller/produtocontroller.php">
  <div class="form-group">
    <label>Nome</label>
    <input type="text" class="form-control" name="nome">
  </div>
  <div class="form-group">
    <label>Descrição</label>
    <input type="text" class="form-control" name="descricao">
  </div>
  <div class="form-group">
    <label>Preço</label>
    <input type="text" class="form-control" name="preco">
  </div>
  <button type="submit" class="btn btn-primary">Adicionar</button>
</form>
</div>
</body>
</html>