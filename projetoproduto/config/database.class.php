<?php

class Database{
	private $host = "localhost";
    private $username = "root";
    private $password = "bancodedados";
    private $dbname = "crud";
    private $conexao = null; 

    public function __construct()
    {          
        $this->conect();
    }

    public function getConection()
    {
        return $this->conexao;
    }

    private function conect() 
    {

    	try{
			$str = "mysql:host=".$this->host.";dbname=".$this->dbname;
			$this->conexao = new PDO($str, $this->username, $this->password);
			$this->conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		   // echo "Connectado com sucesso"; 
		}catch(PDOException $e){
			echo "Erro na conexão: " . $e->getMessage();
		}

    }
}
