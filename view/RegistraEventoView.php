<!DOCTYPE html>
<html>
<head>
	<title>Cadastrar Evento</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../CSS/css.css">
</head>
<body>
	<div class="menu1">
		<ul>
			<li><a href="*">Participante</a>
				<ul>
					<li><a href="*">Criar</a></li>
					<li><a href="*">Alterar</a></li>
					<li><a href="*">Excluir</a></li>
				</ul>
			</li>
			<li><a href="*">Evento</a>
				<ul>
					<li><a href="RegistraEventoView.php">Criar</a></li>
					<li><a href="*">Alterar</a></li>
					<li><a href="*">Excluir</a></li>
				</ul>
			</li>
			<li><a href="*">Listas</a></li>
		</ul>
	</div>
	<br>
	<form action="../controller/eventoControl.php" method="post">
		<div>
			<div>
				<label>Nome</label>
				<input type="text" name="nome" placeholder="Nome" required value="Latinoware">
			</div>
		</div>
		<br>
		<div>
			<div>
				<label>Descrição</label>
				<input type="text" name="descricao" placeholder="Descrição" required value="Boa">
			</div>
			<div>
				<label>Data de Início</label>
				<input type="date" name="inicio" required>
			</div>
			<div>
				<label>Data de Finalização</label>
				<input type="date" name="final" required>
			</div>
		</div>
		<br>
		<div>
			<div class="col-10">
				<button type="submit">Enviar</button>
			</div>
		</div>
		<input type="hidden" name="acao" value="1">
	</form>
</body>
</html>