<?php
	require_once('Conexao.php');
	class EventoDao{
		private $con;
		function __construct(){
			$this->con = Conexao::conectar();
		}
		function inserir(Evento $evento){
			$sql = "INSERT INTO tbevento(Nome, Descricao, DtInicio, DtFim) VALUES (?,?,?,?)";
			$stm = $this->con->prepare($sql);
			//sql injection, onde o numero preenche respectivamente a posição ali nos valores
			$stm-> bindValue(1,$evento->getNome(), PDO::PARAM_STR);
			$stm-> bindValue(2,$evento->getDescricao(), PDO::PARAM_STR);
			$stm-> bindValue(3,$evento->getDtinicio(), PDO::PARAM_STR);
			$stm-> bindValue(4,$evento->getDtfim(), PDO::PARAM_STR);
			$stm->execute();
		}
		//essa está incompleta
		function ver(){
			$sql = "SELECT * FROM tbevento";
			$stm = $this->con->prepare($sql);
			$stm->execute();
		}
		function deleta($nome){
			$sql = "DELETE FROM tbevento WHERE Nome = ?";
			$stm = $this->con->prepare($sql);
			$stm->bindValue(1,$nome, PDO::PARAM_STR);
			$stm->execute();
		}
		function altera(Evento $evento){
			$sql = "UPDATE tbevento SET ID = ?, Nome = ?,Descricao = ?, DtInicio = ?, DtFim = ? WHERE ";
			$stm = $this->con->prepare($sql);
			$stm-> bindValue(1,$evento->getNome(), PDO::PARAM_STR);
			$stm-> bindValue(2,$evento->getDescricao(), PDO::PARAM_STR);
			$stm-> bindValue(3,$evento->getDtinicio(), PDO::PARAM_STR);
			$stm-> bindValue(4,$evento->getDtfim(), PDO::PARAM_STR);
			$stm->execute();
		}
	}
?>