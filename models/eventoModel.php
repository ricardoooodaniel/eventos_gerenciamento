<?php
class Evento{
	private $nome;
	private $descricao;
	private $dtinicio;
	private $dtfim;

	public function getNome(){
		return $this->nome;
	}

	public function setNome($nome){
		$this->nome = $nome;
	}

	public function getDescricao(){
		return $this->descricao;
	}

	public function setDescricao($descricao){
		$this->descricao = $descricao;
	}

	public function getDtinicio(){
		return $this->dtinicio;
	}

	public function setDtinicio($dtinicio){
		$this->dtinicio = $dtinicio;
	}

	public function getDtfim(){
		return $this->dtfim;
	}

	public function setDtfim($dtfim){
		$this->dtfim = $dtfim;
	}
}

?>